package org.example;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class MainTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void add() {

        assertEquals(2,Main.add(1,1));
        assertEquals(50,Main.add(40,10));
        assertEquals(-1,Main.add(-2,1));

    }

    @Test
    void subtract() {
        assertEquals(0,Main.subtract(2,2));
        assertEquals(10,Main.subtract(20,10));
        assertEquals(45,Main.subtract(50,5));
    }

    @Test
    void multiply() {
        assertEquals(40,Main.multiply(4,10));
        assertEquals(25,Main.multiply(5,5));
        assertEquals(4,Main.multiply(2,2));

    }

    @Test
    void divide() {

        assertEquals(2,Main.divide(4,2));
        assertEquals(5,Main.divide(25,5));
        assertEquals(10,Main.divide(100,10));

    }
}