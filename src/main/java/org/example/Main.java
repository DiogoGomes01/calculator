package org.example;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input;

        do {
            System.out.println("Digite sair para sair  \nDigite uma expressão no formato 'número operador número', sem espaços:");
             input = scanner.nextLine();

            char operator = 0;
            for (char c : input.toCharArray()) {
                if (c == '+' || c == '-' || c == '*' || c == '/') {
                    operator = c;
                    break;
                }
            }

            if (operator == 0) {
                System.out.println("Operador inválido. Por favor, use +, -, * ou /.");
                return;
            }

            String[] parts = input.split("[-+*/]");
            if (parts.length != 2) {
                System.out.println("Formato inválido. Por favor, insira a expressão corretamente.");
                return;
            }

            double num1 = Double.parseDouble(parts[0]);
            double num2 = Double.parseDouble(parts[1]);

            double result = 0;
            switch (operator) {
                case '+':
                    result = add(num1, num2);
                    break;
                case '-':
                    result = subtract(num1, num2);
                    break;
                case '*':
                    result = multiply(num1, num2);
                    break;
                case '/':
                    result = divide(num1, num2);
                    break;
            }

            System.out.println("Resultado: " + result);
        }
        while (!input.equalsIgnoreCase("sair"));
    }

    // Métodos de operações matemáticas
    public static double add(double a, double b) {
        return a + b;
    }

    public static double subtract(double a, double b) {
        return a - b;
    }

    public static double multiply(double a, double b) {
        return a * b;
    }

    public static double divide(double a, double b) {
        if (b == 0) {
            System.out.println("Erro: Divisão por zero.");
            return 0;
        }
        return a / b;
    }
}